var http = require('http');
var static = require('node-static');

var config = {
    port: 8888
};

var account = {
    balance: 500
};

var sendJson = function(rep, obj) {
    rep.writeHead(200, 'OK', { "content-type": "application/json" });
    rep.write(JSON.stringify(obj));
    rep.end();    
}

var sendError = function(rep, code, msg) {
    rep.writeHead(code, 'ERROR', { "content-type": "application/json" });
    rep.write(JSON.stringify({ error: msg }));
    rep.end();    
}

var getPayload = function(req, rep, callback) {
    var payload = '';
    req.on("data", function(data) {
        payload += data;
    }).on("end", function() {
        try {
            var obj = JSON.parse(payload);
            callback(rep, obj);
        } catch(ex) {
            callback(rep, { error: "Invalid payload" });
        }
    });
}

var fileServer = new static.Server('./public');
var httpServer = http.createServer();

httpServer.on('request', function (req, rep) {

    console.log(req.method, req.url);
    if(/^\/rest\//.test(req.url)) {
        switch(req.method) {
            case 'GET':
                sendJson(rep, account);
                break;
            case 'POST':
                getPayload(req, rep, function(rep, obj) {
                    if(!obj.error) {
                        if(!isNaN(obj.amount)) {
                            account.balance -= obj.amount;
                            sendJson(rep, account);
                        } else {
                            sendError(rep, 400, "Amount is not a number");
                        }
                    } else {
                        sendError(rep, 400, "Error in transfer data");
                    }    
                });
                break;
            case 'DELETE':
                account.balance = 0;
                sendJson(rep, account);
                break;
            default:
                sendError(rep, 400, "Unhandled method");
            }
    } else {
        fileServer.serve(req, rep);
    }

});

try {
    httpServer.listen(config.port);
    console.log("HTTP server is listening on the port " + config.port);
} catch(ex) {
    console.log("Port " + config.listeningPort + " cannot be used");
    process.exit();
}