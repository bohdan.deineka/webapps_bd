var app = angular.module("pai2020", []);

app.controller("Ctrl1", [ "$http", function($http) {

    var ctrl = this;

    ctrl.amount = 0;
    ctrl.account = {};

    $http.get('/rest/bank').then(
        function(rep) {
            ctrl.account = rep.data;
        },
        function(err) {}
    );

    ctrl.transfer = function(){
        $http.post('rest/bank', { amount : ctrl.amount }).then(
            function(rep) {
                ctrl.account = rep.data;
            },
            function(err) {}
        );
    }

    ctrl.clear = function(){
        $http.delete('rest/bank').then(
            function(rep) {
                ctrl.account = rep.data;
                ctrl.amount = 0;
            },
            function(err) {}
        );
    }
}]);